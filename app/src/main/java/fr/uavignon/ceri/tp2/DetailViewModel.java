package fr.uavignon.ceri.tp2;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;
import fr.uavignon.ceri.tp2.data.BookRepository;


public class DetailViewModel extends AndroidViewModel
{
    private BookRepository bookRepository;
    private MutableLiveData<Book> book;

    public DetailViewModel(@NonNull Application application)
    {
        super(application);

        bookRepository = new BookRepository(application);
        book = bookRepository.getSelectedBook();
    }

    MutableLiveData<Book> getBook() { return book; }


    public void insertOrUpdateBook( Book book )
    {
        // bookRepository.deleteBook(-1);
        if( book.getId() == -1)
        {
            book.setId( bookRepository.getBooksCount() + 1 );
            Log.e("", "INSERT");
            bookRepository.insertBook(book);
        }
        else
        {
            Log.e("", "UPDATE");
            bookRepository.updateBook(book);
        }
    }

    public void setSelectedBook(long id) {
        bookRepository.getBook(id);
        book = bookRepository.getSelectedBook();
    }


}
