package fr.uavignon.ceri.tp2;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

public class DetailFragment extends Fragment {

    private DetailViewModel viewModel;
    private EditText textTitle, textAuthors, textYear, textGenres, textPublisher;
    private Button updateButton;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);
        updateButton = view.findViewById(R.id.buttonUpdate );

        // Get selected book
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        viewModel.setSelectedBook((int)args.getBookNum());
        // Book book = Book.books[(int)args.getBookNum()];

        textTitle = (EditText) view.findViewById(R.id.nameBook);
        textAuthors = (EditText) view.findViewById(R.id.editAuthors);
        textYear = (EditText) view.findViewById(R.id.editYear);
        textGenres = (EditText) view.findViewById(R.id.editGenres);
        textPublisher = (EditText) view.findViewById(R.id.editPublisher);

        observerSetup();

        view.findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });

        view.findViewById(R.id.buttonUpdate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick( View view ) {

                // Book(String title, String authors, String year, String genres, String publisher)


             String title = textTitle.getText().toString();
             String authors = textAuthors.getText().toString();
             String year = textYear.getText().toString();
             String genres = textGenres.getText().toString();
             String pub = textPublisher.getText().toString();

             int id = (int)args.getBookNum();

             // Log.e("ERROR", "id: " + id );

             Book upbook = new Book( title, authors, year, genres, pub );
             upbook.setId( id );

             viewModel.insertOrUpdateBook( upbook );


             NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                         .navigate(R.id.action_SecondFragment_to_FirstFragment);

            }
        });

    }

    private void observerSetup()
    {
        viewModel.getBook().observe(getViewLifecycleOwner(), new Observer<Book>()
        {
            @Override
            public void onChanged(Book book)
            {
                if( book != null && book.getId() != -1 )
                {
                    textTitle.setText(book.getTitle());
                    textAuthors.setText(book.getAuthors());
                    textYear.setText(book.getYear());
                    textGenres.setText(book.getGenres());
                    textPublisher.setText(book.getPublisher());
                }
                else
                {
                    updateButton.setText("Ajouter");
                }
            }
        });
    }

}