package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;
import fr.uavignon.ceri.tp2.data.BookRepository;



public class ListViewModel extends AndroidViewModel
{
    private BookRepository bookRepository;
    private LiveData<List<Book>> allBooks;



    public ListViewModel(@NonNull Application application)
    {
        super(application);

        bookRepository = new BookRepository(application);
        allBooks = bookRepository.getAllBooks();
    }

    LiveData<List<Book>> getAllBooks() { return allBooks; }

    /*
    public void getBook( long id ){ bookRepository.getBook( id ); }
    public void insertBook( Book book ){ bookRepository.insertBook( book );}
    public void updateBook( Book book ){ bookRepository.updateBook( book );}

    public void deleteBook( long id ){ bookRepository.deleteBook( id );}
    public void deleteAllBooks(){ bookRepository.deleteAllBooks();}
     */

}
